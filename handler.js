'use strict';

const xml2js = require('xml2js');

module.exports.helloWorld = (event, context, callback) => {
  xml2js.parseString(event.data.message, (err, data) => {
    if (err) {
      return callback(err);
    }

    // do some validation

    const { message } = data;

    const newMessage = {
      ...message,
      from: message.to,
      to: message.from
    };

    const xmlBuilder = new xml2js.Builder();

    callback(null, xmlBuilder.buildObject({
      message: newMessage
    }));
  });
};
